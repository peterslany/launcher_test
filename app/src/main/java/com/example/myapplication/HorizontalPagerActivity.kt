package com.example.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2

/**
 * The number of pages (wizard steps) to show in this demo.
 */
private const val NUM_PAGES = 3

class HorizontalPagerActivity : FragmentActivity() {

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */

    private lateinit var mPager: ViewPager2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.horizontal_pager)

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = findViewById(R.id.horizontalPager)
        // !!! The pager adapter, which provides the pages to the view pager widget.
        val pagerAdapter = HorizontalPagerAdapter(this)
        mPager.adapter = pagerAdapter

        // nastavi ako vychoziu poziciu 1 cize home screen
        mPager.currentItem = 1
    }

    override fun onBackPressed() {
        if (mPager.currentItem == 1) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed()
        } else {
            // Otherwise, select the previous step.
            mPager.currentItem = 1 //mPager.currentItem - 1
        }
    }

    /**
     * Adapter for horizontal pager
     *
     */
    private inner class HorizontalPagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
        override fun getItemCount(): Int = NUM_PAGES

        override fun createFragment(position: Int): Fragment {
            return when(position) {
                0 ->  BreathworkScreenFragment()
                1 ->  VerticalPagerFragment(mPager)
                2 ->  AppDrawerScreenFragment()
                else -> Fragment()
            }
        }
    }
}


