package com.example.myapplication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class AppDrawerScreenFragment : Fragment () {
    private var recyclerView: RecyclerView? = null
    private var adapter: AppItemAdapter? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) : View {
        val view = inflater.inflate(R.layout.app_drawer_fragment, container, false)
        recyclerView =
            view.findViewById(R.id.appDrawerRecycler)
                    as RecyclerView
        adapter = AppItemAdapter(view.context)
        val layoutManager =
            LinearLayoutManager(activity?.applicationContext)
        recyclerView!!.layoutManager = layoutManager
        recyclerView!!.itemAnimator = DefaultItemAnimator()
        // Add a neat dividing line between items in the list
       recyclerView!!.addItemDecoration(
            DividerItemDecoration(view.context,
                LinearLayoutManager.VERTICAL)
        )
        // set the adapter
        recyclerView!!.adapter = adapter
        return view;
    }

}class BreathworkScreenFragment : Fragment () {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.breathwork_screen_fragment, container, false)
}

class MainScreenFragment : Fragment () {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.main_screen_fragment, container, false)
}

class PersonalScreenFragment : Fragment () {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.personal_screen_fragment, container, false)
}
