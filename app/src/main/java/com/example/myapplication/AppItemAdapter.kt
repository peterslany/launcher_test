package com.example.myapplication

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView


class AppInfo {
    var label: CharSequence? = null
    var packageName: CharSequence? = null
    var icon: Drawable? = null
}

class AppItemAdapter(c: Context):
    RecyclerView.Adapter<AppItemAdapter.AppItemHolder>() {

    private var appsList: ArrayList<AppInfo>? = ArrayList()

    init {
        //This is where we build our list of app details, using the app
        //object we created to store the label, package name and icon


        //This is where we build our list of app details, using the app
        //object we created to store the label, package name and icon
        val pm: PackageManager = c.getPackageManager()
        appsList = ArrayList()

        val i = Intent(Intent.ACTION_MAIN, null)
        i.addCategory(Intent.CATEGORY_LAUNCHER)

        val allApps = pm.queryIntentActivities(i, 0)
        for (ri in allApps) {
            val app = AppInfo()
            app.label = ri.loadLabel(pm)
            app.packageName = ri.activityInfo.packageName
            app.icon = ri.activityInfo.loadIcon(pm)
            appsList!!.add(app)
        }
    }
    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int): AppItemHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.app_item, parent, false)
        return AppItemHolder(itemView)
    }

    override fun getItemCount(): Int {
        if (appsList != null) {
            return appsList!!.size
        }
        // error
        return -1
    }

    override fun onBindViewHolder(
        holder: AppItemHolder, position: Int) {

        val app = appsList!![position]
        holder.appName.text = app.label.toString()
        holder.appIcon.setImageDrawable(app.icon)
    }

    inner class AppItemHolder(view: View) :
        RecyclerView.ViewHolder(view),
        View.OnClickListener {

        internal var appName =
            view.findViewById<View>(
                R.id.appDrawerAppName) as TextView

        internal var appIcon =
            view.findViewById<View>(
                R.id.appDrawerAppIcon) as ImageView

        init {

            view.isClickable = true
            view.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            val pos = adapterPosition
            val context: Context = view.getContext()

            val launchIntent = context.packageManager
                .getLaunchIntentForPackage(appsList!![pos].packageName.toString())
            context.startActivity(launchIntent)
            Toast.makeText(view.getContext(), appsList!![pos].label.toString(), Toast.LENGTH_LONG)
                .show()

        }

    }
}