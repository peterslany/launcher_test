package com.example.myapplication


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2


/**
 * The number of pages (wizard steps) to show in this demo.
 */
private const val NUM_PAGES = 2



class VerticalPagerFragment(other: ViewPager2) : Fragment() {
    val outerViewPager : ViewPager2 = other

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private lateinit var mPager: ViewPager2
        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ) : View {
            return inflater.inflate(R.layout.vertical_pager, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Instantiate a ViewPager and a PagerAdapter.
        mPager = view!!.findViewById(R.id.verticalPager)
        // zaregistrovanie onPageChange callbacku, pre blokovanie swipovania pri presunuti sa do personal page
        mPager.registerOnPageChangeCallback(VerticalPagerOnPageChangeCallback())
        // !!! The pager adapter, which provides the pages to the view pager widget.
        val pagerAdapter = HomePagerAdapter(this)
        mPager.adapter = pagerAdapter

        mPager.currentItem = 0
    }

    // override kvoli back press funkcionalite
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // pri stlaceni back buttonu sa vrati na main screen
                mPager.currentItem = 0
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    private inner class HomePagerAdapter(f: Fragment) : FragmentStateAdapter(f) {

        override fun getItemCount(): Int = NUM_PAGES
        override fun createFragment(position: Int): Fragment {
            return when(position) {
                0 ->  MainScreenFragment()
                1 ->  PersonalScreenFragment()
                else -> Fragment()
            }
        }
    }

    inner class VerticalPagerOnPageChangeCallback : ViewPager2.OnPageChangeCallback() {

        override fun onPageSelected(position: Int) {
            if (position == 1) {
                outerViewPager.isUserInputEnabled = false;

                Log.i("HOME:pageselected", ": $position")
            } else {
                outerViewPager.isUserInputEnabled = true
            }
        }

    }
}


